'use strict';
(function () {
    $.ajax({
        type: 'GET',
        url: 'data.json',
        success: function(data){  
            var source = $("#menu-items").html();
            var template = Handlebars.compile(source);     
            var html = template(data);
            $('#menu-container').append(html);
    }});
    
   
})()